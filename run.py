import os
from flask import jsonify
from application import create_app

app = create_app(os.getenv('FLASK_CONFIG') or 'default')


@app.route('/api/help')
def help():
    """
    Print available functions with their URL mappings and docstrings
    """
    func_list = {}
    for rule in app.url_map.iter_rules():
        if rule.endpoint != 'static':
            func_list[rule.rule] = app.view_functions[rule.endpoint].__doc__
    return jsonify(func_list)

