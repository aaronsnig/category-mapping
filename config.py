"""
Application configuration
"""

import os

BASEDIR = os.path.abspath(os.path.dirname(__file__))


class Config:
    """
    Base configuration
    """
    SECRET_KEY = os.getenv('FLASK_SECRET_KEY')
    SSL_DISABLE = False
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL',
                                        'sqlite:///' + BASEDIR + '/db.db')

    @staticmethod
    def init_app(app):
        pass

config = {
    'default': Config
}