# Categories App

## Getting The Application Up & Running

In order to run this app, you will need to install the requirements by running the following commands:

```bash
virtualenv env # to create the virtual environment
. env/bin/activate # activate the env
pip install -r requirements.txt # install the requirements
```

Once this has been completed you can set the *FLASK_APP* to tell Flask which file will be run:

```bash
export FLASK_APP=run.py # Linux/Mac OS X
set FLASK_APP=run.py # Windows
```

Once that's done you'll be able to run the app with:

```bash
flask run
```

If you wish to run this in debug mode, be sure to run:

```bash
export FLASK_DEBUG=1 # Linux/Mac OS X
set FLASK_DEBUG=1 # Windows
```

## Working With The Database

### The Setup
The database settings are configured in the `Config` object in the `config.py` file. This defines the location of the database. By default, this is configured to read the database URI from an environment variable, or default to a file called `db.db` in the root directory of the project.

### Migrations
The database uses **Flask-Migrate** to handle the database migrations. In order to acheive this a new `db` manager was created using **Flask-Script**.

The most common commands needed to work with **Flask-Migrate** are:

```bash
flask db init # Initialise the migration directory
flask db migrate # Register the migrations
flask db upgrade # Apply the latest migration to the database
```

### Data Structure
The database configuration of this application consists of a single database table.

The data is structured as follows:

```
    +----+-------------+-----------+
    | id |    name     | parent_id |
    +----+-------------+-----------+
    |1   |Mens         |NULL       |
    |2   |Womens       |NULL       |
    |3   |Clothing     |NULL       |
    |4   |Jewellery    |NULL       |
    |5   |Jeans        |3          |
    |6   |Watches      |4          |
    +----+-------------+-----------+
```

The `parent_id` field is used to reference the `id` field of the same table. This is known as an *Adjacent List Relationship*.

Using this type of relationship we can keep all of the various categories in the same table. An alternative to this approach would be to create a separate table for subcategories, possibly a *modified preorder*, however this would likely result in duplicate data, as such:

```
    - Mens
        |
        |---- Clothing
                  |
                  |---- Jeans
    - Women
        |
        |---- Clothing
                  |
                  |---- Jeans

```

Using *adjacent list* approach we can just reference any parents that we may need to use.

### Querying The Data
The route used to access the view that will return this data is comprised of two parts - the main category (either *Mens* or *Womens*), followed by a subcategory.

The top level category is retrieved based the name of the main category contained within the URL.

The next part of this process is to get the subcategory from the URL and retrieve a category with that `name` from the database.

For example, if the URL `Mens/Clothing` is used - the *Mens* part of the URL will be retrieved as the top level category and then the category with the name of *Clothing* will be retrieved.

If the subcategory contains a `parent_id` then the program will enter the recursive part of the program in the `get_parents` function. This function will retrieve the `category` that contains an `id` equal to the *current categories* `parent_id`.

As per the example above, if we were to use the URL `/Mens/Jeans`, then the category with the name of *Jeans* would be retrieved. A check would then be performed to see if it has a `parent_id`. In this case the *Jeans* category has a `parent_id` of 3. The recursive nature of this function then kicks in. The `parent_id` (3) is passed to the function and a query is performed to retrieve the category that has an id of 3. Due to the tree-like of this structure, this will keep recursively retrieving parents so long as a `parent_id` is available.

## Exposing the data
Each time a piece of information is retrieved from the database, it is added to an `OrderedDict`. This will preserve the order in which the items are added to the dict. This dict is then reversed to convey the order of the items - i.e. Mens -> Clothing -> Jeans:

```
{
  "data": [
    [
      "Mens", 
      {
        "id": 1, 
        "name": "Mens", 
        "parent_id": null
      }
    ], 
    [
      "Clothing", 
      {
        "id": 3, 
        "name": "Clothing", 
        "parent_id": ""
      }
    ], 
    [
      "Jeans", 
      {
        "id": 6, 
        "name": "Jeans", 
        "parent_id": 3
      }
    ]
  ]
}
```

This data is the serialised to JSON using Flask's `jsonify` function.

This, however, is not the best approach. Ideally each child should be a nested object inside of it's parent.

## Tests
Tests can be found in the `category` app and can be run from the root directory of the project using:

```bash
python -m unittest discover application/category/
```

## Documentation
Given the size of the project, the documentation contained in this README file, as well as the docstrings and comments should be sufficient.

I addition to this, a rudimentary documentation page is available based on the docstrings of the registered URLs.

This page can be accessed at `/api/help`