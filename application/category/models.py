"""
The category app's models
"""
from application import db


class Category(db.Model):
    """
    Category Model

    Stores information about each category
    """
    __tablename__ = "category"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    parent_id = db.Column(db.Integer)

    def as_dict(self):
        return {c.name: getattr(
            self, c.name) for c in self.__table__.columns}

    def __str__(self):
        return self.name
