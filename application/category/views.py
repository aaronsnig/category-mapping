"""
The main module for this project.

This module is comrised of a number of functions that will parse, map and
serialise data to present the mapping of the category structure.

In order to use this functionality, a request should be made to the
<category>/<subcategory> URL.

For example - https://127.0.0.1:5000/Mens/Jeans
"""


from collections import OrderedDict
from flask import jsonify
from application.category import categories
from application.category.models import Category


def get_top_level_parent(category_name):
    """
    Some categories may have multiple parents. For example, `Clothing` could
    have a `Mens` & `Womens` parent categories and we need to make this
    determination based off the `category` contained in the route.

    :param: category_name
    :return: parent
    """
    return Category.query.filter_by(name=category_name).first()


def get_parents(parent_id, parents, category):
    """
    Recursively retrieve each parent of a given category

    :param: parent_id
    :param: parents
    :param: category
    :return: parents
    """

    # Retrieve the current category based on the `parent_id`
    current_category = Category.query.filter_by(id=parent_id).first()

    # Add this parent to the `parents` dict so it can be jsonified
    parents.update({current_category.name: current_category.as_dict()})

    # If there is a `parent_id` value present, call this function with
    # the `parent_id` of the `current_category`, as well as the current
    # `parents` dict
    if current_category.parent_id:
        get_parents(current_category.parent_id, parents, category)

    return parents


def get_subcategory(category, subcategory):
    """
    Get the relevant parents of a specific subcategory

    :param: category
    :param: subcategory
    :return: parents
    """
    # Get the specific subcategory
    sub_cat = Category.query.filter_by(name=subcategory).first()

    # Get top level parent
    parent = get_top_level_parent(category)

    # Create a list of the parent IDs
    parent_ids = sub_cat.parent_id

    parents = OrderedDict()

    # Add the sub category to the `parents` dict
    parents.update({sub_cat.name: sub_cat.as_dict()})

    # Use the `get_parents` function to recursively retrieve each
    # parent of each child
    if sub_cat.parent_id:
        parents.update(get_parents(parent_ids, parents, category))

    # Add top level parent to the `parents` dict
    parents.update({parent.name: parent.as_dict()})

    return OrderedDict(reversed(parents.items()))


@categories.route('/<category>/<subcategory>')
def index(category, subcategory):
    """
    Get the subcategory & it's relevant parent categories

    :param: category
    :param: subcategory
    :return: jsonified mapping
    """
    mapping = get_subcategory(category, subcategory)
    return jsonify(data=list(mapping.items()))
