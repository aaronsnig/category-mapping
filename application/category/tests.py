"""

This module holds the test case used to test the category functionality.BaseException

In order to run these tests you can run:
    $ python -m unittest discover application/category/

All of the tests in this module will then be run.
"""
import os
import unittest
from collections import OrderedDict
from application import create_app, db
from application.category.models import Category
from application.category.views import get_parents
from application.category.views import get_top_level_parent
from application.category.views import get_subcategory


class TestCase(unittest.TestCase):
    """
    Test cases for the category mapping application
    """

    app = create_app(os.getenv('FLASK_CONFIG') or 'default')

    def test_parent_returns_correct_value_for_single_parent(self):
        """
        Invoking the `get_parents` function with a child category that
        contains a single parent, the function returns the mapping sequence
        """
        with self.app.app_context():
            mapping = get_parents(6, OrderedDict(), "Mens")
            self.assertEqual(len(mapping), 2)

    def test_parent_returns_correct_value_for_multiple_parents(self):
        """
        Invoking the `get_parents` function with a child category that
        contains a multiple parents, the function returns the mapping sequence
        """
        with self.app.app_context():
            mapping = get_parents(3, OrderedDict(), "Womens")
            self.assertEqual(len(mapping), 1)

    def test_top_level_parent_is_returned(self):
        """
        Invoking the `get_top_level_parent` function will return a value
        of two after being called with an arguement of `Womens`
        """
        with self.app.app_context():
            parent = get_top_level_parent("Womens")
            self.assertEqual(parent.id, 2)

    def test_subcategory_that_has_no_parents(self):
        """
        Invoking the `get_subcategory` function will return a top level
        category and a single sub-category
        """
        with self.app.app_context():
            mapping = get_subcategory("Mens", "Clothing")
            self.assertEqual(len(mapping), 2)
            self.assertEqual(mapping["Mens"]["name"], "Mens")
            self.assertEqual(mapping["Clothing"]["name"], "Clothing")

    def test_subcategory_with_multiple_ancestors(self):
        """
        Invoking the `get_subcategory` function will return a top level
        category, the sub category contained within the URL, and any
        intermediary categories
        """
        with self.app.app_context():
            mapping = get_subcategory("Mens", "Jeans")
            self.assertEqual(len(mapping), 3)
            self.assertEqual(mapping["Mens"]["name"], "Mens")
            self.assertEqual(mapping["Clothing"]["name"], "Clothing")
            self.assertEqual(mapping["Jeans"]["name"], "Jeans")
