"""
The main application initialisation used to create the Flask app, initialise
Flask extensions and register any blueprints
"""
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from config import config

db = SQLAlchemy()


def create_app(config_name):
    """
    Create the Flask app instance based on the configuration name and
    register blueprints

    :param: config_name
    :return: app
    """

    # Initialise the basic Flask application using `config_name` as the
    # configuration
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    # Initialise the SQLAlchemy instance
    db.init_app(app)

    # Create the `migrate` and `manager` instances to allow
    # the for the ability to run the migration commands
    migrate = Migrate(app, db)
    manager = Manager(app)
    manager.add_command('db', MigrateCommand)

    # Register category blueprint
    from application.category import categories
    app.register_blueprint(categories)

    # Import `Category` model to allow alembic to see the model
    from application.category.models import Category

    return app
